from calendar import timegm
from math import sqrt
from multiprocessing import cpu_count
from time import gmtime

from numpy import floor, square, sum, array, random
from scipy import sparse, rand

from fastembed.fast_embed import FastEmbed



class TestFastEmbed:
    @staticmethod
    def test_defaults(mat, fun, embed_dims):
        """
        Checks if FastEmbed works with default configuration
        :param mat: matrix to embed
        :param fun: embedding function
        :param embed_dims: dimensionality of compressive embedding
        :return: FastEmbed obj
        """
        embed = FastEmbed(mat, fun, embed_dims)

        # check if correct defaults are getting picked up
        assert embed.poly_order == FastEmbed.POLY_ORDER
        assert embed.boost == FastEmbed.BOOST
        assert embed.scale_up_spec_norm_est == FastEmbed.SCALE_UP_SPEC_NORM_EST
        assert embed.spec_norm_est_iter == FastEmbed.SPEC_NORM_EST_ITER
        assert embed.spec_norm_est_dims == FastEmbed.SPEC_NORM_EST_DIMS
        assert embed.normalize_fun == FastEmbed.NORMALIZE_FUN
        assert embed.n_jobs == cpu_count()
        assert embed.embed.shape == (mat.shape[0], embed_dims)

        return embed

    @staticmethod
    def test_custom(mat, fun, embed_dims, config, phi):
        """
        Checks if FastEmbed works with custom configurations
        :param mat: matrix to embed
        :param fun: embedding function
        :param embed_dims: dimensionality of compressive embedding
        :param config: configuration parameters
        :param phi: function to weight polynomial approximation errors
        :return: FastEmbed object
        """
        embed = FastEmbed(mat, fun, embed_dims, config, phi)

        # check if custom config is getting picked up
        assert all(getattr(embed, key) == val for key, val in config.items() if (key, val) != ('n_jobs', -1))
        if 'n_jobs' in config and config['n_jobs'] == -1:
            assert embed.n_jobs == cpu_count()

        assert embed.embed.shape == (mat.shape[0], embed_dims)

        return embed

    def __init__(self):
        # generate a large sparse matrix for checks
        n = 5000
        m = 2 * n

        random.seed(timegm(gmtime()))

        u = array([i % n for i in range(m)])
        v = array([int(floor(n * rand(1, 1))) for _ in range(m)])
        d = array([1.] * m)

        mat = sparse.coo_matrix((d, (u, v)), shape=(n, n)).tocsr()
        mat = mat + mat.transpose()
        embed_dims = 80

        def fun_1(x):
            return 1. * (x > -0.3)

        def fun_2(x):
            return 1. * (x > 0.6)

        embed_1 = self.test_defaults(mat, fun_1, embed_dims)
        embed_2 = self.test_defaults(mat, fun_2, embed_dims)

        # sanity check on energy in the two embeddings using the fact that fun_1
        # captures more eigenvectors (is a superset) than fun_2
        sum_1 = sum(sum(square(embed_1.embed))) / embed_dims
        sum_2 = sum(sum(square(embed_2.embed))) / embed_dims
        assert sum_1 > sum_2

        # perform same checks with custom configurations
        config_1 = {'n_jobs': 1, 'poly_order': 61, 'boost': 3, 'verbose': True}
        config_2 = {'spec_norm_est_dims': 41, 'spec_norm_est_iter': 65, 'poly_order': 61,
                    'scale_up_spec_norm_est': 1.02, 'n_jobs': -1, 'boost': 3, 'verbose': True}

        def phi(x):
            return 1. / sqrt(1. + 1e-2 - x * x)

        embed_3 = self.test_custom(mat, fun_1, embed_dims, config_1, phi)
        embed_4 = self.test_custom(mat, fun_2, embed_dims, config_2, phi)

        # sanity check on energy in the two embeddings using the fact that fun_1
        # captures more eigenvectors (is a superset) than fun_2
        sum_3 = sum(sum(square(embed_3.embed))) / embed_dims
        sum_4 = sum(sum(square(embed_4.embed))) / embed_dims

        assert sum_3 > sum_4


if __name__ == '__main__':
    TestFastEmbed()
