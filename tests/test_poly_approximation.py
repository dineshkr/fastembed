from fastembed.polynomials.polynomial_approximator import PolynomialApproximator
from numpy import square, sqrt, abs
from scipy.integrate import quad


class TestPolynomialApproximator:
    EPS = 1e-6

    @staticmethod
    def _error_eval(fun_approx_obj):
        """
        Worker method to compute errors
        """
        fun = fun_approx_obj.fun
        phi = fun_approx_obj.phi
        fun_approx = fun_approx_obj.eval_approx
        error = quad(lambda x: phi(x) * square(fun(x) - fun_approx(x)), -1, 1, limit=100)[0]
        error_self_eval = fun_approx_obj.approx_error
        return error, error_self_eval

    @staticmethod
    def _tester(fun, phi, max_order):
        """
        Driver method that performs tests: approximation error must be non-increasing with order of approximation
         The PolynomialApproximator's linear algebra based estimate of error should be close to numerical estimate of
         error
        """
        errors = []
        fun_approx_obj = PolynomialApproximator(fun, phi, 0)
        errors.append(TestPolynomialApproximator._error_eval(fun_approx_obj))
        for r in range(1, max_order + 1):
            fun_approx_obj.add_order()
            errors.append(TestPolynomialApproximator._error_eval(fun_approx_obj))

        err, err_self_eval = zip(*errors)
        assert all(err[i] <= err[i - 1] + TestPolynomialApproximator.EPS for i in range(1, len(err)))
        assert all(err_self_eval[i] <= err_self_eval[i - 1] + TestPolynomialApproximator.EPS
                   for i in range(1, len(err_self_eval)))
        assert all(abs(x - y) < TestPolynomialApproximator.EPS for x, y in zip(err, err_self_eval))

    def __init__(self):
        max_order = 40

        def fun(x):
            return 1. * (x > 0.5)

        # two different methods of measuring approximation error ->
        # minimize integrate( phi_1(x) * (fun(x) - fun_poly_aprox_1(x))^2, -1, 1) and
        # minimize integrate( phi_2(x) * (fun(x) - fun_poly_aprox_2(x))^2, -1, 1)

        def phi_1(x):
            return 1.

        def phi_2(x):
            return 1. / sqrt(1. + 1e-3 - x * x)

        TestPolynomialApproximator._tester(fun, phi_1, max_order)
        TestPolynomialApproximator._tester(fun, phi_2, max_order)


if __name__ == '__main__':
    TestPolynomialApproximator()
