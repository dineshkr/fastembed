from functools import partial

from fastembed.polynomials.lanczos_polynomial import LanczosPolynomial
from numpy import abs, sqrt


class TestLanczosPolynomial:
    EPS = 1e-10

    @staticmethod
    def _test_inner_prod(lanczos_polynomials, i, j):
        """
        Checks if the Lanczos series is orthogonal
        """
        other_poly = partial(lanczos_polynomials.evaluate_poly, r=j)
        inner_prod = lanczos_polynomials.inner_product(other_poly, i)
        if i == j:
            assert abs(inner_prod - 1.) <= TestLanczosPolynomial.EPS
        else:
            assert abs(inner_prod) <= TestLanczosPolynomial.EPS

    @staticmethod
    def _test_all(phi, max_order):
        """
        Driver method that builds up the polynomial series and invokes orthogonality tests
        """
        lanczos_polynomials = LanczosPolynomial(phi, 0)
        TestLanczosPolynomial._test_inner_prod(lanczos_polynomials, 0, 0)
        for j in range(1, max_order + 1):
            lanczos_polynomials.add_order()
            for i in range(0, j + 1):
                TestLanczosPolynomial._test_inner_prod(lanczos_polynomials, i, j)

    def __init__(self):
        max_order = 30

        # two different notions of orthogonality

        def phi_1(x):
            return 1.

        def phi_2(x):
            return 1. / sqrt(1. - x * x)

        self._test_all(phi_1, max_order)
        self._test_all(phi_2, max_order)


if __name__ == '__main__':
    TestLanczosPolynomial()
