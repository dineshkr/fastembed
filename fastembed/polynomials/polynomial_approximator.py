from numpy import square
from scipy.integrate import quad

from lanczos_polynomial import LanczosPolynomial


class PolynomialApproximator:
    """
    Container for polynomial approximation of a function
    """

    def __init__(self, fun, phi, order):
        """
        Best r-th order polynomial approximation of fun as measured by:
            integral in -1 to 1 of ( phi(x) (fun(x) - polynomial_approximation(x))^2 )
        :param fun: function for which we seek a polynomial approximation
        :param phi: function that defines the inner product space (needs to be positive in [-1, 1])
        :param order: order of the polynomial approximation
        :return: None
        """
        self.fun = fun
        self.phi = phi

        self.lanczos_series = None
        self.order = None
        self.lanczos_coeff = None
        result = quad(lambda x: self.phi(x) * square(fun(x)), -1, 1, limit=100)
        squared_norm = result[0]
        self.approx_error = squared_norm

        self._perform_approximation(order)

    def _perform_approximation(self, order):
        """
        Driver method that obtains the projection of fun onto a Lanczos polynomial series
        and populates the order, lanczos_series, approx_error and lanczos_coeff fields
        """
        # zeroth order approximation first
        self.lanczos_series = LanczosPolynomial(self.phi, 0)
        self.order = 0
        new_coeff = self.lanczos_series.inner_product(self.fun, 0)
        self.lanczos_coeff = [new_coeff]
        self.approx_error -= new_coeff * new_coeff

        # build up to requested order
        for r in range(1, order + 1):
            self.add_order()

    def approx_details(self):
        """
        :return: information needed to write down the polynomial approximation
        """
        return self.lanczos_series.alpha, self.lanczos_series.beta, self.lanczos_coeff

    def _eval_approx(self, x, r):
        """
        evaluates the polynomial approximation of order r @ x
        """
        current = 1. / self.lanczos_series.beta[0]

        grand_parent = 0.
        parent = current
        cum_eval = self.lanczos_coeff[0] * current

        def eval_current(i, x_i, x_i_minus_1):
            return ((x - self.lanczos_series.alpha[i - 1]) * x_i - self.lanczos_series.beta[i - 1] * x_i_minus_1) \
                   / self.lanczos_series.beta[i]

        # recurse to r-th order polynomial
        for j in range(1, r + 1):
            current = eval_current(j, parent, grand_parent)
            grand_parent = parent
            parent = current
            cum_eval += self.lanczos_coeff[j] * current

        return cum_eval

    def eval_approx(self, x, r=None):
        """
        evaluates the polynomial approximation of order r @ x
        :param x: point where we want the approximation evaluated
        :param r: order of the polynomial approximation (defaults to the largest order available)
        """
        if r is None:
            r = self.order

        if r < 0:
            raise ValueError("order must be positive")
        elif r > self.order:
            raise ValueError("order cannot be greater than the polynomial approximation order")
        elif r > len(self.lanczos_coeff) - 1:
            raise ValueError("This approximation is not yet available")

        return self._eval_approx(x, r)

    def add_order(self):
        """
        Improve the polynomial approximation by increasing its order
        """
        # get the next order in the lanczos polynomial series
        self.lanczos_series.add_order()

        # project out what has been approximated so far
        # though this is not needed (we can self.fun instead of residual when we compute new_coeff below)
        # we leave this in for stability
        def residual(x):
            return self.fun(x) - self._eval_approx(x, self.order)

        # compute inner product of the residual with the newly extracted (self.order + 1)-th \
        # order Lanczos polynomial
        new_coeff = self.lanczos_series.inner_product(residual, self.order + 1)

        # update polynomial approximation using the inner product
        self.lanczos_coeff.append(new_coeff)
        self.order += 1
        self.approx_error -= new_coeff * new_coeff
