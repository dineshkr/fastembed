from numpy import square, sqrt
from scipy.integrate import quad


class LanczosPolynomial:
    """
    Container for Lanczos polynomial:
        Sequence of polynomials of increasing order (by one) that are mutually orthogonal 
        as per some inner product space. They can be evaluated via a 3-term recursion and 
        this class holds the polynomial and associated evaluation methods. They span all 
        polynomials upto the specified order
    """

    def __init__(self, phi, order):
        """
        Builds up the Lanczos polynomial series up to the requested order
        :param phi: function that defines the inner product space 
                             (needs to be non-negative in [-1, 1])
        :param order: max order of lanczos series
        :return: none
        """
        if order < 0:
            ValueError("order must be no smaller than 0")

        self.phi = phi

        # initialize Lanczos series with zeroth order unit-norm polynomial
        result = quad(self.phi, -1, 1)
        beta_squared = result[0]
        self.alpha = []
        self.beta = [sqrt(beta_squared)]
        self.r = 0

        # recurse up to specified order
        for i in range(1, order + 1):
            self.add_order()

    @staticmethod
    def _evaluate_poly(x, alpha, beta, r):
        """
        Computes the r-th order Lanczos polynomial defined by the
        3-recursion from lists alpha and beta
        :param x: position where we wish to evaluate the Lanczos polynomial
        :param alpha: defines the Lanczos recursion
        :param beta: defines the Lanczos recursion
        :param r: order of the polynomial; r <= len(alpha)
        :return: evaluate r-th order Lanczos polynomial @ x
        """

        parent = 1. / beta[0]
        grand_parent = 0.

        def eval_current(i, x_i, x_i_minus_1):
            return ((x - alpha[i - 1]) * x_i - beta[i - 1] * x_i_minus_1) / beta[i]

        # recurse to r-th order polynomial
        for j in range(1, r + 1):
            current = eval_current(j, parent, grand_parent)
            grand_parent = parent
            parent = current

        return parent

    def evaluate_poly(self, x, r=None):
        """
        Calls the evaluation of the r-th order Lanczos polynomial defined by
        the 3-recursion from lists alpha and beta at x
        :param x: position where this evaluation ought to be performed
        :param r: order of the Lanczos series (Defaults to the highest order available)
        :return: value of r-th polynomial at x
        """
        if r is None:
            r = self.r
        elif r is -1:
            return 0.
        elif r > self.r:
            raise ValueError("Supply order less than or equal to class instance's order {0}".
                             format(self.r))

        return self._evaluate_poly(x, self.alpha, self.beta, r)

    def inner_product(self, fun, r):
        """
        Evaluate inner product of the function fun with the r-th order Lanczos polynomial
         corresponding to self.phi
        :param fun: function
        :param r: order of Lanczos polynomial
        :return: inner product value
        """
        if r > self.r:
            ValueError("Requested order {0} exceeds polynomials that have "
                       "been generated {1}".format(r, self.r))

        result = quad(lambda x: fun(x) * self.evaluate_poly(x, r) * self.phi(x), -1, 1, limit=100)
        inner_product = result[0]

        return inner_product

    def add_order(self):
        """
        Increase order of Lanczos approximation
        :return: None
        """

        # subtracting beta[r-1] times r-1-th order lanczos polynomial ensures
        # that  x * r-th order Lanczos polynomial is orthogonal to r-1-th
        # order lanczos polynomial
        def next_lanczos_poly(x):
            return x * self.evaluate_poly(x, self.r) - self.beta[-1] * self.evaluate_poly(x, self.r - 1)

        # compute inner product with r-th order lanczos polynomial
        alpha_new = self.inner_product(next_lanczos_poly, self.r)

        # remove projection along the r-th order lanczos polynomial
        def next_lanczos_poly_orth(x):
            return (x - alpha_new) * self.evaluate_poly(x, self.r) - self.beta[-1] * self.evaluate_poly(x, self.r - 1)

        # compute beta_new = norm as per ip space of next_Lanczos_poly_orth
        result = quad(lambda x: self.phi(x) * square(next_lanczos_poly_orth(x)), -1, 1, limit=100)
        beta_new_squared = result[0]
        beta_new = sqrt(beta_new_squared)

        # update container
        self.alpha.append(alpha_new)
        self.beta.append(beta_new)
        self.r += 1

    def order(self):
        return self.r
