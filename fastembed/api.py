from numpy import copy
from scipy import sparse
from fast_embed import FastEmbed


def fast_embed_eig(mat, fun, embed_dims, config=None, phi=None):
    """
    Wrapper for computing eigenvector embeddings a symmetric matrix using the FastEmbed class
    :param mat: symmetric matrix for which we seek an eigenvector embedding
    :param fun: How to weight eigenvectors relative to their eigenvalue / maximum abs(eigenvalue)
    :param embed_dims: size of embedding
    :param config: ***** Optional **** configuration parameters - a dict can take keys in:
        'poly_order' - What order of the polynomial approximation approximation of the embedding function 'fun' is
                                needed? Depends on how sharp the function 'fun' is and the size of the matrix. For high
                                dimensional matrices "errors" in approximation can add up. So larger values of 'poly_order'
                                are recommended
        'boost' - How to approximate 'fun'? If nulls in the spectrum are important (which is typically the case when we
                                want to suppress noise), we recommend setting boost to 2 or 3 (as opposed to 1). In this
                                case, we obtain the polynomial approximation of fun(x) ^ (1/boost) and cascade the
                                 algorithm 'boost' number of times. See the paper in the README for details
        'sigma_max' - The spectral norm of the matrix 'mat' computed elsewhere. If this parameter is specified we do not
                                attempt to estimate the spectral norm of the matrix and hence the parameters
                                'spec_norm_est_dims', 'spec_norm_est_iter' and 'scale_up_spec_norm_est' are unused
        'spec_norm_est_dims' - Number of random starting vectors to use to estimate the spectral norm of the matrix
                                via power iteration
        'spec_norm_est_iter' - Number of iterates of power iteration for estimating the spectral norm (depends on how
                                clustered the eigen values are around the spectral norm)
        'scale_up_spec_norm_est' - After estimating the spectral norm using power iteration (this is strictly a lower
                                bound on the spectral norm), we multiply it by a small number greater than 1 given by
                                scale_up_spec_norm_est (defaults to FastEmbed.SCALE_UP_SPEC_NORM_EST) to potentially
                                arrive at an upper bound. Since the algorithm assumes that we have an upper bound on the
                                spectral norm of the matrix, this parameter alongside 'spec_norm_est_dims',
                                'spec_norm_est_iter' may have to be tuned if the algorithm diverges.
                                Another option in those cases is to provide the spectral norm computed elsewhere
                                using the parameter sigma_max
        'n_jobs' - Number of cores to use in embedding computation (defaults to -1, which denotes all available cores)
        'normalize_fun' - if set to True (default), fun represents the scaling of the eigen value relative to the
                                spectral norm (largest magnitude eigenvalue) ie,
                                f(eigenvalue/spectralnorm) * eigenvector; if set to False, we use fun as is on the
                                eigenvalue to obtain its weight ie., f(eigenvalue) * eigenvector
         'verbose' - if set to True - prints details along the way; Defaults to False
    :param phi **** Optional ****
        how to weight polynomial approximation errors
        integrate(phi(x) * (fun(x) - poly_approx_of_fun(x))^2, -1, 1) (Defaults to phi(x) = 1.)
    :return: Embedding of rows/cols of size (#rows, embed_dims) of mat
    """
    embed = FastEmbed(sparse.csr_matrix(mat), fun, embed_dims, config, phi)
    embed_rows_cols = copy(embed.embed)
    return embed_rows_cols


def fast_embed_svd(mat, fun, embed_dims, config=None, phi=None):
    """
    Wrapper for computing SVD embeddings using the FastEmbed class
    :param mat: matrix for which we seek an SVD embedding
    :param fun: How to weight singular vectors relative to their singular value / maximum singular value
    :param embed_dims: Size of embedding
    :param config: ***** Optional **** configuration parameters - a dict can take keys in:
        'poly_order' - What order of the polynomial approximation approximation of the embedding function 'fun' is
                                needed? Depends on how sharp the function 'fun' is and the size of the matrix. For high
                                dimensional matrices "errors" in approximation can add up. So larger values of 'poly_order'
                                are recommended
        'boost' - How to approximate 'fun'? If nulls in the spectrum are important (which is typically the case when we
                                want to suppress noise), we recommend setting boost to 2 or 3 (as opposed to 1). In this
                                case, we obtain the polynomial approximation of fun(x) ^ (1/boost) and cascade the
                                 algorithm 'boost' number of times. See the paper in the README for details
        'sigma_max' - The spectral norm of the matrix 'mat' computed elsewhere. If this parameter is specified we do not
                                attempt to estimate the spectral norm of the matrix and hence the parameters
                                'spec_norm_est_dims', 'spec_norm_est_iter' and 'scale_up_spec_norm_est' are unused
        'spec_norm_est_dims' - Number of random starting vectors to use to estimate the spectral norm of the matrix
                                via power iteration
        'spec_norm_est_iter' - Number of iterates of power iteration for estimating the spectral norm (depends on how
                                clustered the singular values are around the spectral norm)
        'scale_up_spec_norm_est' - After estimating the spectral norm using power iteration (this is strictly a lower
                                bound on the spectral norm), we multiply it by a small number greater than 1 given by
                                scale_up_spec_norm_est (defaults to FastEmbed.SCALE_UP_SPEC_NORM_EST) to potentially
                                arrive at an upper bound. Since the algorithm assumes that we have an upper bound on the
                                spectral norm of the matrix, this parameter alongside 'spec_norm_est_dims',
                                'spec_norm_est_iter' may have to be tuned if the algorithm diverges.
                                Another option in those cases is to provide the spectral norm computed elsewhere
                                using the parameter sigma_max
        'n_jobs' - Number of cores to use in embedding computation (defaults to -1, which denotes all available cores)
        'normalize_fun' - if set to True (default), fun represents the scaling of the singular value relative to the
                                spectral norm (largest magnitude singular value) ie,
                                f(singularvector/spectralnorm) * singularvector; if set to False, we use fun as is on
                                the singularvalue to obtain its weight ie., f(singularvalue) * singularvector
         'verbose' - if set to True - prints details along the way; Defaults to False
    :param phi **** Optional ****
        how to weight polynomial approximation errors
        integrate(phi(x) * (fun(x) - poly_approx_of_fun(x))^2, -1, 1) (Defaults to phi(x) = 1.)
    :return: a tuple with embedding of (rows, cols) of size (#rows, embed_dims) and (#columns, embed_dims)
    """
    m = mat.shape[0]
    n = mat.shape[1]

    mat = sparse.csr_matrix(mat)

    # extra memory : 2*mem(mat)
    sym_mat = sparse.vstack([sparse.hstack([sparse.csr_matrix((n, n)), mat.transpose(copy=True)]),
                             sparse.hstack([mat, sparse.csr_matrix((m, m))])]).tocsr()

    def fun_prime(x):
        return fun(x) * (x >= 0) - fun(-x) * (x < 0)

    sym_embed = FastEmbed(sym_mat, fun_prime, embed_dims, config, phi)
    embed_rows = sym_embed.embed[range(n, m + n), :]
    embed_cols = sym_embed.embed[range(n), :]

    return embed_rows, embed_cols
