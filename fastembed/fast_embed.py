from multiprocessing import Pool, cpu_count

import numpy as np
from scipy import sparse

from matrix_utils import compute_embed, spectral_norm
from polynomials.polynomial_approximator import PolynomialApproximator


class FastEmbed:
    """
    Class that contains the compressed embedding obtained using the FastEmbed algorithm and the associated parameters
    """
    BOOST = 2
    POLY_ORDER = 60

    @staticmethod
    def _default_phi(_):
        return 1.

    SCALE_UP_SPEC_NORM_EST = 1.01
    SPEC_NORM_EST_ITER = 80
    SPEC_NORM_EST_DIMS = 20
    NORMALIZE_FUN = True
    N_JOBS = -1
    VERBOSE = False
    CONFIG_INPUT = {'spec_norm_est_dims', 'spec_norm_est_iter', 'poly_order', 'scale_up_spec_norm_est', 'n_jobs',
                    'sigma_max', 'boost', 'normalize_fun', 'verbose'}

    def __init__(self, mat, fun, embed_dims, config=None, phi=None):
        """
        Embedding object for symmetric matrices
        :param mat: A square symmetric sparse scipy matrix in CSR format
        :param fun: Spectral weighting function used to weight eigenvectors
        :param embed_dims: Number of dims for the embedding
        :param phi: Approximation error weighting function
        :param config: (Optional) Dict with algorithm configuration parameters
        :return: FastEmbed object
        """
        # we need csr matrix for mat-vec multiplications
        if not isinstance(mat, sparse.csr_matrix):
            raise ValueError("Input matrix must be a square symmetric scipy "
                             "sparse symmetric matrix")

        self.mat = mat
        self.n = mat.shape[0]
        self.fun = fun
        self.normalize_fun = FastEmbed.NORMALIZE_FUN
        self.embed_dims = embed_dims
        if phi is None:
            self.phi = FastEmbed._default_phi
        else:
            self.phi = phi

        self.poly_order = FastEmbed.POLY_ORDER
        self.boost = FastEmbed.BOOST

        self.spec_norm_est_dims = FastEmbed.SPEC_NORM_EST_DIMS
        self.spec_norm_est_iter = FastEmbed.SPEC_NORM_EST_ITER
        self.scale_up_spec_norm_est = FastEmbed.SCALE_UP_SPEC_NORM_EST
        self.n_jobs = FastEmbed.N_JOBS
        self.sigma_max = None
        self.verbose = FastEmbed.VERBOSE

        if config is not None:
            self._load_config(config)

        if self.n_jobs == -1:
            self.n_jobs = cpu_count()

        if self.sigma_max is None:
            self.sigma_max = self._est_sigma_max()

        self.poly_approx = self._approx_poly(fun, self.boost, self.sigma_max, self.normalize_fun, self.phi,
                                             self.poly_order)

        if self.verbose:
            print ("{0} concurrent jobs will be used".format(self.n_jobs))
            print ("Spectral norm of matrix is {0}".format(self.sigma_max))
            print ("Polynomial approximation error is {0}".format(self.poly_approx.approx_error))
            print ("Lanczos polynomial series recursion\nalpha {}\nbeta {}".format(
                self.poly_approx.lanczos_series.alpha,
                self.poly_approx.lanczos_series.beta,
            ))
            print ("Lanczos mixing weights {}".format(self.poly_approx.lanczos_coeff))
            print ("Proceeding to compute embedding ...")

        self.proj_mat = (2. * np.random.randint(2, size=(self.n, self.embed_dims)) - 1.) / \
                        float(np.sqrt(self.embed_dims))
        self.embed = None
        # Fill up self.embed
        self._eval_embed()

    def _load_config(self, config):
        """
        Loads parameters specified via the config dict
        """

        for key in FastEmbed.CONFIG_INPUT:
            if key in config:
                setattr(self, key, config[key])

    @staticmethod
    def _approx_poly(fun, boost, sigma_max, normalize_fun, phi, poly_order):
        """
        Sets up the polynomial approximation of the spectral weighting function fun using the relevant parameters
        """
        if normalize_fun:
            if boost is 1:
                return PolynomialApproximator(fun, phi, poly_order)
            elif boost > 1:
                def fun_prime(x):
                    return np.power(np.abs(fun(x)), 1. / boost) * np.sign(fun(x))

                return PolynomialApproximator(fun_prime, phi, poly_order)
            else:
                raise ValueError('Boost parameter cannot be smaller than 1')

        if boost is 1:
            def fun_prime(x):
                return fun(1. * x / sigma_max)
            PolynomialApproximator(fun_prime, phi, poly_order)
        elif boost > 1:
            def fun_prime(x):
                y = 1. * x / sigma_max
                return np.power(np.abs(fun(y)), 1. / boost) * np.sign(fun(y))
            PolynomialApproximator(fun_prime, phi, poly_order)
        else:
            raise ValueError('Boost parameter cannot be smaller than 1')

    def _est_sigma_max(self):
        """
            Sets up a parallel implementation of spectral norm estimation via Power Iteration
            using Python's multiprocessing library (Driver method)
        """

        start_vecs = 2 * np.random.randint(2, size=(self.n, self.spec_norm_est_dims)) - 1

        n_jobs = self.n_jobs
        n_jobs = min(n_jobs, self.spec_norm_est_dims)

        if n_jobs == 1:
            # no need the over head of a new process
            sigma_max_vecs = spectral_norm((self.mat, start_vecs, self.spec_norm_est_iter))
            sigma_max = self.scale_up_spec_norm_est * float(max(sigma_max_vecs))
            del start_vecs
            return sigma_max

        pool = Pool(processes=n_jobs)

        # slice projection matrix (pick columns)
        each_dim = self.spec_norm_est_dims // n_jobs
        indices = [range(r * each_dim, min((r + 1) * each_dim, self.spec_norm_est_dims)) for r in range(n_jobs)]
        indices[-1].extend(range(each_dim * n_jobs, self.spec_norm_est_dims))

        # different arguments for each processor
        args_list = [(self.mat, start_vecs[:, indices[r]], self.spec_norm_est_iter) for r in range(n_jobs)]

        # map followed by reduce step (concatenate results together)
        sigma_max_vecs = np.concatenate(pool.map(spectral_norm, args_list), axis=0)

        # kill processes
        pool.terminate()

        return self.scale_up_spec_norm_est * float(max(sigma_max_vecs))

    def _eval_embed(self):
        """
        Sets up a parallel implementation of FastEmbed using python's multiprocessing library
        """
        n_jobs = self.n_jobs
        n_jobs = min(n_jobs, self.embed_dims)

        if n_jobs == 1:
            # no need the over head of a new process
            self.embed = compute_embed((self.mat, self.sigma_max, np.copy(self.proj_mat), self.boost,
                                        self.poly_approx.lanczos_coeff, self.poly_approx.lanczos_series.alpha,
                                        self.poly_approx.lanczos_series.beta, self.verbose))
            return

        # create n_jobs processors
        pool = Pool(processes=n_jobs)

        # slice projection matrix (pick columns)
        each_dim = self.embed_dims // n_jobs
        indices = [range(r * each_dim, min((r + 1) * each_dim, self.embed_dims))
                   for r in range(n_jobs)]
        indices[-1].extend(range(each_dim * n_jobs, self.embed_dims))

        # different arguments for each processor

        args = [(self.mat, self.sigma_max, self.proj_mat[:, indices[r]], self.boost,
                 self.poly_approx.lanczos_coeff, self.poly_approx.lanczos_series.alpha,
                 self.poly_approx.lanczos_series.beta, self.verbose) for r in range(n_jobs)]

        self.embed = np.concatenate(pool.map(compute_embed, args), axis=1)
