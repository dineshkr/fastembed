import numpy as np
from scipy.sparse import csc_matrix, csr_matrix


def spectral_norm(args):
    """
    runs power iteration on the columns of vecs to estimate the spectral norm of a symmetric matrix
    :param args: (mat, vecs, iter_max)
            mat: a square sparse scipy matrix
            vecs: A numpy matrix of the same length as A
            iter_max: number of iterations
    :return: An array of lower bounds of the spectral norm of the symmetric matrix mat
    """
    # unpack arguments
    mat, vecs, iter_max = args
    del args

    if mat.shape[0] != mat.shape[1]:
        raise ValueError('Matrix must be square')

    sigma_max_est = 0

    for iter_count in range(iter_max):
        vecs_new = mat * vecs
        vecs_new_norm = np.linalg.norm(vecs_new, axis=0)

        if iter_count == (iter_max - 1):
            vecs_norm = np.linalg.norm(vecs, axis=0)
            sigma_max_est = [y / x for (x, y) in zip(vecs_norm, vecs_new_norm)]

        vecs = vecs_new * csc_matrix(([1.0 / x for x in vecs_new_norm], (range(vecs.shape[1]), range(vecs.shape[1]))))

    return sigma_max_est


def compute_embed(args):
    """
    Worker function which computes the embedding
    :param args: tuple (mat, mat_spec_norm, proj_mat, boost, lanczos_coeff, alpha, beta, verbose)
    :return: embedding matrix of same size as as embed_mat
    """

    mat, mat_spec_norm, proj_mat, boost, lanczos_coeff, alpha, beta, verbose = args

    del args

    if not isinstance(mat, csr_matrix):
        raise ValueError('Matrix must be a sparse CSR matrix')

    if mat.shape[0] != mat.shape[1]:
        raise ValueError('Matrix must be square')

    if mat.shape[1] != proj_mat.shape[0]:
        raise ValueError('Projection vectors must match matrix size')

    if boost <= 0:
        raise ValueError('Boost parameter must be a positive integer')

    # core lanczos iteration
    def eval_current(i, x_i, x_i_minus_1):
        # return (mat*x_i/mat_spec_norm - alpha[i-1]*x_i - beta[i-1]*x_i_minus_1)/beta[i]
        a = (1. / (mat_spec_norm * beta[i])) * (mat * x_i)
        b = (beta[i - 1] / beta[i]) * x_i_minus_1
        if alpha[i - 1] == 0:
            return a - b
        else:
            return a - b - (alpha[i - 1] / beta[i]) * x_i

    embedding = proj_mat

    for boost_idx in range(boost):
        current_vectors = embedding / beta[0]
        # zeroth order polynomial approximation of embedding function
        embedding = lanczos_coeff[0] * current_vectors

        if verbose:
            ndims = proj_mat.shape[1]
            energy_lanczos = np.sum(np.square(current_vectors)) / ndims
            energy_embedding = np.sum(np.square(embedding)) / ndims
            print ("Boost {}; Lanczos {}; Energy Lanczos {}; Energy embedding {}".format(
                boost_idx + 1, 0, energy_lanczos, energy_embedding))

        parent_vectors = current_vectors
        grand_parent_vectors = np.zeros(current_vectors.shape)

        # recurse to r-th order polynomial approximation of
        # spectral embedding
        for r in range(1, len(lanczos_coeff)):
            current_vectors = eval_current(r, parent_vectors, grand_parent_vectors)
            embedding += lanczos_coeff[r] * current_vectors

            if verbose:
                ndims = proj_mat.shape[1]
                energy_lanczos = np.sum(np.square(current_vectors)) / ndims
                energy_embedding = np.sum(np.square(embedding)) / ndims
                print ("Boost {}; Lanczos {}; Energy Lanczos {}; Energy embedding {}".format(
                    boost_idx + 1, r, energy_lanczos, energy_embedding))

            grand_parent_vectors = parent_vectors
            parent_vectors = current_vectors

    return embedding
