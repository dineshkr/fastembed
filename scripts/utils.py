class Indexer:
    def __init__(self):
        self.word_idx = dict()
        self.idx_word = dict()
        self.counter = 0

    def add(self, word):
        if word not in self.word_idx:
            self.word_idx[word] = self.counter
            self.idx_word[self.counter] = word
            self.counter += 1


def save_embeddings(indexer, embed_rows, output_fname):
    """

    :param indexer:
    :param embed_rows:
    :param output_fname:
    :return:
    """
    import numpy
    assert isinstance(indexer, Indexer)
    assert isinstance(embed_rows, numpy.ndarray)
    with open(output_fname, 'w') as fout:
        m, n = embed_rows.shape
        for i in range(m):
            line = indexer.idx_word[i] + ' ' + ','.join(map(str, embed_rows[i, :].tolist()))
            fout.write(line + '\n')


def load_adjacency_mat(fname, sep=","):
    """

    :param fname: File with edges in csv format (nodeId1, nodeId2, weight)
    :param sep: csv separator (default comma)
    :return: adjacency matrix
    """

    if len(open(fname).next().split(sep)) == 2:
        u_ids, v_ids = zip(*[line.split(sep) for line in open(fname)])
        w = [1.] * len(u_ids)
    else:
        u_ids, v_ids, d = zip(*[line.split(sep) for line in open(fname)])
        w = map(float, d)
    indexer = Indexer()
    u = []
    v = []
    for node_id1, node_id2 in zip(u_ids, v_ids):
        indexer.add(node_id1)
        indexer.add(node_id2)
        u.append(indexer.word_idx[node_id1])
        v.append(indexer.word_idx[node_id2])
    return indexer, u, v, w


def word_context_mat(fname):
    """

    :param fname: File with text snippets
    :return: Positive Pointwise Mutual Information Matrix (PPMI)
    """
    import re
    import math

    def form_tuples(words):
        return [
            ((word, context), 1. / abs(i - j))
            for i, word in enumerate(words) for j, context in enumerate(words)
            if i != j and word != context and abs(i - j) <= 5
        ]

    word_context_count = dict()
    word_count = dict()
    context_count = dict()
    total = 0

    for line in open(fname):
        for key, value in form_tuples(map(lambda word: word.lower().strip(), re.sub(r'\W+', ' ', line).split())):
            word, context = key
            word_context_count[key] = word_context_count.setdefault(key, 0) + value
            word_count[word] = word_count.setdefault(word, 0) + value
            context_count[context] = context_count.setdefault(context, 0) + value
            total += value

    ppmi = dict()
    for key in word_context_count:
        word, context = key
        ppmi_value = math.log(word_context_count[key] * total / word_count[word] / context_count[context])
        if ppmi_value > 0:
            ppmi[key] = ppmi_value

    indexer = Indexer()
    u = []
    v = []
    w = []
    for key in ppmi:
        word, context = key
        indexer.add(word)
        indexer.add(context)
        u.append(indexer.word_idx[word])
        v.append(indexer.word_idx[context])
        w.append(ppmi[key])
    return indexer, u, v, w