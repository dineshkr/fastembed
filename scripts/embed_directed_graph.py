def parse_args():
    from argparse import ArgumentParser
    parser = ArgumentParser("Create node embeddings")
    parser.add_argument('-f', '--file', help='File with edges in either as nodeId1,nodeId2 or nodeId1,nodeId2,weight',
                        required=True, type=str)
    parser.add_argument('-o', '--output_file', help='Output file with node embeddings', required=True, type=str)
    parser.add_argument('--embed_dims', help='Number of embed dims', required=False, type=int, default=40)
    parser.add_argument('--threshold', help='Parameter to optimize for different graphs in the range (0, 1)',
                        required=False, type=float, default=0.95)
    return parser.parse_args()


def main():
    import numpy as np
    from scipy import sparse
    from fastembed.api import fast_embed_svd
    from utils import save_embeddings, load_adjacency_mat

    args = parse_args()

    indexer, u, v, w = load_adjacency_mat(args.file)
    n = indexer.counter
    mat = sparse.coo_matrix((w, (u, v)), shape=(n, n)).tocsr()

    degrees = mat.sum(axis=1).transpose().tolist()[0]
    d = sparse.coo_matrix(([1. / x if x > 0 else 0 for x in degrees], (range(n), range(n))),
                          shape=(n, n)).tocsr()
    rw_prob_trans_mat = d.dot(mat)

    embed_rows, embed_cols = fast_embed_svd(
        mat=rw_prob_trans_mat,
        fun=lambda x: 1. if abs(x) > args.threshold else 0.,
        embed_dims=args.embed_dims,
        config={
            'poly_order': 60,
            'boost': 2,
            'verbose': True
        }
    )

    save_embeddings(indexer, np.concatenate([embed_rows, embed_cols], axis=1), args.output_file)


if __name__ == '__main__':
    main()
