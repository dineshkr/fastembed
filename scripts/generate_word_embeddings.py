def parse_args():
    from argparse import ArgumentParser
    parser = ArgumentParser("Create word embeddings")
    parser.add_argument('-f', '--file', help='File with sentences', required=True, type=str)
    parser.add_argument('-o', '--output_file', help='File with word embeddings', required=True, type=str)
    parser.add_argument('--embed_dims', help='Number of embed dims', required=False, type=int, default=30)
    parser.add_argument('--threshold', help='Parameter to optimize for different embeddings', required=False, type=float,
                        default=0.025)
    return parser.parse_args()


def main():
    from scipy import sparse
    from fastembed.api import fast_embed_svd
    from utils import save_embeddings, word_context_mat

    args = parse_args()

    # form PPMI matrix
    indexer, u, v, w = word_context_mat(args.file)
    n = indexer.counter
    mat = sparse.coo_matrix((w, (u, v)), shape=(n, n)).tocsr()

    embed_rows, _ = fast_embed_svd(
        mat=mat,
        fun=lambda x: abs(x) > args.threshold,
        embed_dims=args.embed_dims,
        config={
            'poly_order': 100,
            'boost': 3,
            'spec_norm_est_dims': 10,
            'spec_norm_est_iter': 40,
            'normalize_fun': True,
            'verbose': True
        }
    )
    save_embeddings(indexer, embed_rows, args.output_file)


if __name__ == '__main__':
    main()
