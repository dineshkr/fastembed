def parse_args():
    from argparse import ArgumentParser
    parser = ArgumentParser("Create node embeddings")
    parser.add_argument('-f', '--file', help='File with edges in either as nodeId1,nodeId2 or nodeId1,nodeId2,weight',
                        required=True, type=str)
    parser.add_argument('-o', '--output_file', help='Output file with node embeddings', required=True, type=str)
    parser.add_argument('--embed_dims', help='Number of embed dims', required=False, type=int, default=40)
    parser.add_argument('--threshold', help='Parameter to optimize for different graphs in the range (-1, 1)',
                        required=False, type=float, default=0.95)
    return parser.parse_args()


def main():
    import math
    from scipy import sparse
    from fastembed.api import fast_embed_eig
    from utils import save_embeddings, load_adjacency_mat

    args = parse_args()

    indexer, u, v, w = load_adjacency_mat(args.file)
    n = indexer.counter
    mat = sparse.coo_matrix((w, (u, v)), shape=(n, n)).tocsr()
    # ensure symmetric
    mat = (mat + mat.transpose()) * 0.5

    degrees = mat.sum(axis=0).tolist()[0]
    d = sparse.coo_matrix(([1. / math.sqrt(x) if x > 0 else 0 for x in degrees], (range(n), range(n))),
                          shape=(n, n)).tocsr()
    normalized_adjacency_mat = d.dot(mat).dot(d)

    embed = fast_embed_eig(
        mat=normalized_adjacency_mat,
        fun=lambda x: 1. if x > args.threshold else 0.,
        embed_dims=args.embed_dims,
        config={
            'poly_order': 60,
            'boost': 2,
            'sigma_max': 1.,
            'verbose': True
        }
    )
    save_embeddings(indexer, embed, args.output_file)


if __name__ == '__main__':
    main()
