# README #

### Summary ###

* Python library for quick computation of compressed SVD embeddings
* [D. Ramasamy and U. Madhow. "Compressive spectral embedding: sidestepping the SVD," Neural Information Processing Systems 2015](http://papers.nips.cc/paper/5992-compressive-spectral-embedding-sidestepping-the-svd)

## Large datasets ##
Spark version of this package available in https://bitbucket.org/dineshkr/fastembedspark

### Overview ###
This package provides means to compute approximate SVD embeddings in the following setting:

* High dimensional data and the need for SVD embeddings that include the effect of a large number of singular vectors make conventional SVD embedding computation expensive
* The cost function for downstream inference methods can be shown to depend on data samples only via pairwise euclidean distances between data samples or metrics derived from these pairwise distances like the inner product. e.g, SVM and k-means 
* These downstream inference methods are robust to small distortions in pairwise distances among data samples introduced by compressive embeddings

### Usage ###

* ``pip install -r requirements.txt``
* ``python setup.py clean install``
* See the examples in the ``scripts`` folder

### Citing ###

If you find FastEmbed useful for your research, please consider citing the paper

```
@inproceedings{fastembed-nips2015,
author = {Ramasamy, Dinesh and Madhow, Upamanyu},
 title = {Compressive spectral embedding: sidestepping the SVD},
 booktitle = {Neural Information Processing Systems},
 year = {2015}
}

```